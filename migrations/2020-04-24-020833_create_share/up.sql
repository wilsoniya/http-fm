-- Your SQL goes here
CREATE TABLE fs_share (
  id INTEGER PRIMARY KEY NOT NULL,
  path VARCHAR NOT NULL,
  share_id VARCHAR(36) NOT NULL,
  deleted boolean NOT NULL
);

create UNIQUE INDEX idx_fs_share_share_id ON fs_share(share_id);
