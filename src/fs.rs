use std::fs;
use std::path::Path;
use tokio;

use serde::Serialize;

use crate::errors::HFMError;

/// An item that appears in a filesystem; either a file or a directory.
pub enum FSItem {
    /// The contents of a directory
    Directory(DirectoryListing),
    /// An open file and its length
    File(tokio::fs::File, u64),
}

impl FSItem {
    pub async fn new(path: &Path) -> Result<Self, HFMError> {
        match tokio::fs::metadata(path).await {
            Ok(metadata) => {
                if metadata.is_file() {
                    tokio::fs::File::open(path).await
                        .map_err(HFMError::from)
                        .map(|file| Self::File(file, metadata.len()))
                } else if metadata.is_dir() {
                    fs::read_dir(path)
                        .map_err(HFMError::from)
                        .and_then(|dir_entries: fs::ReadDir| {
                            dir_entries
                                .map(|maybe_dir_entry: Result<fs::DirEntry, _>| {
                                    maybe_dir_entry
                                        .map_err(HFMError::from)
                                        .and_then(<DirItem as std::convert::TryFrom<std::fs::DirEntry>>::try_from)
                                })
                            .collect()
                        })
                    .map(|items| Self::Directory(DirectoryListing { items }))
                } else {
                    Err(HFMError::UnknownFileType)
                }
            },
            Err(err) => Err(HFMError::from(err)),
        }
    }

    /// Returns a copy of this instance with the path adjusted to replace
    /// `prefix` with `replacement`. Only affects instances of the `Directory`
    /// variant.
    pub fn contextualize(self, prefix: &str, replacement: &str) -> Self {
        match self {
            Self::Directory(directory_listing) => {
                Self::Directory(DirectoryListing {
                    items: directory_listing.items
                        .iter()
                        .map(|dir_item| {
                            let mut new_dir_item = (*dir_item).to_owned();
                            let path = new_dir_item.get_path();
                            let new_path = replace_prefix(path, &prefix, replacement);
                            new_dir_item.set_path(new_path);
                            new_dir_item
                        })
                        .collect()
                })
            }
            Self::File(..) => self,
        }
    }
}

/// Returns a copy of `string` with the first instance of `prefix` substituted
/// with `replacement`.
fn replace_prefix(string: &str, prefix: &str, replacement: &str) -> String {
    string.replacen(prefix, replacement, 1)
}

#[derive(Serialize)]
pub struct DirectoryListing {
    pub items: Vec<DirItem>,
}

#[derive(Clone, Debug, PartialEq, Serialize)]
pub enum DirItem {
    File {
        path: String,
        size_bytes: u64,
    },
    Directory {
        path: String,
    }
}

impl DirItem {
    fn get_path(&self) -> &str {
        match self {
            Self::File{ref path, ..} => path,
            Self::Directory{ref path} => path,
        }
    }

    fn set_path(&mut self, new_path: String) {
        match self {
            Self::File{ref mut path, ..} => *path = new_path,
            Self::Directory{ref mut path} => *path = new_path,
        };
    }
}

impl std::convert::TryFrom<std::fs::DirEntry> for DirItem {
    type Error = HFMError;

    fn try_from(dir_entry: std::fs::DirEntry) -> Result<Self, Self::Error> {
        dir_entry
            .path()
            .to_str()
            .ok_or(HFMError::UnicodeError)
            .and_then(|path| {
                dir_entry.metadata()
                    .map_err(Self::Error::from)
                    .and_then(|meta| {
                        if meta.is_file() {
                            Ok(DirItem::File {
                                path: path.into(),
                                size_bytes: meta.len(),
                            })
                        } else if meta.is_dir() {
                            Ok(DirItem::Directory {
                                path: path.into(),
                            })
                        } else {
                            Err(HFMError::UnknownFileType)
                        }
                    })
            })
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn replace_prefix_test() {
        assert_eq!(String::from("bazbar"), super::replace_prefix("foobar", "foo", "baz"));
    }

    #[test]
    fn fsitem_contextualize_test() {
        use super::{DirectoryListing, DirItem, FSItem};

        let input =  FSItem::Directory(DirectoryListing{
            items: vec![
                DirItem::File{
                    path: String::from("foobar"),
                    size_bytes: 420,
                },
                DirItem::Directory{
                    path: String::from("foobaz"),
                },
            ],
        });
        let actual = input.contextualize("foo", "____");

        if let FSItem::Directory(DirectoryListing{items}) = actual {
            assert_eq!(DirItem::File {
                path: String::from("____bar"),
                size_bytes: 420,
            }, items[0]);
            assert_eq!(DirItem::Directory{
                path: String::from("____baz"),
            }, items[1]);
        } else {
            assert!(false, "FSItem is not a Directory");
        }
    }
} /* test */
