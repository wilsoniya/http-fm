use std::path::PathBuf;

use actix_web::{
    HttpRequest,
    HttpResponse,
    Responder,
    web,
};
use bytes::Bytes;
use futures::future::{ready, Ready};
use futures::stream::StreamExt;
use tokio_util::codec::{BytesCodec, FramedRead};
use tera;

use crate::data;
use crate::errors::HFMError;
use crate::fs;

lazy_static! {
    static ref TERA: tera::Tera = {
        let mut _tera = tera::Tera::default();
        _tera.add_raw_template("directory-listing", include_str!("../templates/directory-listing.html"))
            .expect("unable to load template");
        _tera
    };
}

pub struct State {
    db: data::actor::Db,
}

impl State {
    pub fn new(db: data::actor::Db) -> Self {
        Self { db }
    }
}

impl Responder for fs::DirectoryListing {
    type Error = HFMError;
    type Future = Ready<Result<HttpResponse, Self::Error>>;

    fn respond_to(self, req: &HttpRequest) -> Self::Future {
        let accept = req
            .headers()
            .get("Accept")
            .and_then(|header_val| header_val.to_str().ok())
            .unwrap_or("text/html");

        let response = if accept.contains("application/json") {
            Ok(HttpResponse::Ok().json(self))
        } else {
            tera::Context::from_serialize(&self)
                .map_err(HFMError::from)
                .and_then(|ref context| {
                    let body = TERA.render("directory-listing", context)?;
                    let response = HttpResponse::Ok().content_type("text/html").body(body);
                    Ok(response)
                })
        };

        ready(response)
    }
}

impl Responder for fs::FSItem {
    type Error = HFMError;
    type Future = Ready<Result<HttpResponse, HFMError>>;

    fn respond_to(self, req: &HttpRequest) -> Self::Future {
        match self {
            Self::Directory(directory_listing) => {
                directory_listing.respond_to(req)
            },
            Self::File(file, file_length) => {
                let stream = FramedRead::new(file, BytesCodec::new())
                    .map(|maybe_bytesmut| maybe_bytesmut.map(Bytes::from));
                let response = HttpResponse::Ok()
                    .content_length(file_length)
                    .streaming(stream);
                ready(Ok(response))
            },
        }
    }
}

pub async fn share(req: HttpRequest, data: web::Data<State>) -> impl Responder {
    let _id: String = req.match_info().get("id")
        .ok_or(HFMError::NotFound)?
        .to_string();
    let fpath: &str = req.match_info().query("fpath");

    let maybe_share_path: Option<PathBuf> = data.db
        .get_share(_id).await?
        .map(|fsshare| {
            match fpath.len() {
                0 => PathBuf::from(fsshare.path),
                _ => PathBuf::from(fsshare.path).join(fpath),
            }
        });

    match maybe_share_path {
        Some(share_path) => fs::FSItem::new(&share_path).await.map(|fs_item| {
            fs_item.contextualize(&share_path.to_string_lossy(), &req.uri().to_string())
        }),
        None => Err(HFMError::NotFound),
    }
}

