use std::convert;
use std::io;

use actix_web::http::StatusCode;
use actix_web::web::{
    HttpResponse
};
use diesel::result::{ConnectionError, Error as DieselError};
use tera;

#[derive(Debug)]
pub enum HFMError {
    CLIError(String),
    ConfigError(String),
    ConfigMissingError,
    IOError {
        inner: std::io::Error,
    },
    UnicodeError,
    UnknownFileType,
    DBError(Option<DieselError>),
    ActorError(actix::MailboxError),
    NotFound,
    InvalidPath,
    TemplateError(tera::Error),
}

impl HFMError {
    fn get_message_status(&self) -> (String, actix_web::http::StatusCode) {
        match self {
            HFMError::CLIError(err_msg) => (format!("Error: {}", err_msg), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::ConfigError(err_msg) => (format!("Configuration error: {}", err_msg), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::ConfigMissingError => ("Configuration file does not exist.".into(), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::IOError{inner} => {
                match inner.kind() {
                    io::ErrorKind::NotFound => ("File not found".into(), StatusCode::NOT_FOUND),
                    _ => ("Disk error".into(), StatusCode::INTERNAL_SERVER_ERROR)
                }
            },
            HFMError::UnicodeError{..} => ("Unicode error".into(), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::UnknownFileType{..} => ("Unknown file type".into(), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::DBError(_) => ("Data store error".into(), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::ActorError(_) => ("Internal server error".into(), StatusCode::INTERNAL_SERVER_ERROR),
            HFMError::NotFound => ("Not found".into(), StatusCode::NOT_FOUND),
            HFMError::InvalidPath => ("Invalid Path".into(), StatusCode::BAD_REQUEST),
            HFMError::TemplateError(_) => ("Internal server error".into(), StatusCode::INTERNAL_SERVER_ERROR),
        }
    }
}

impl convert::From<io::Error> for HFMError {
    fn from(other: io::Error) -> Self {
        Self::IOError{ inner: other }
    }
}

impl convert::From<ConnectionError> for HFMError {
    fn from(_other: ConnectionError) -> Self {
        Self::DBError(None)
    }
}

impl convert::From<DieselError> for HFMError {
    fn from(other: DieselError) -> Self {
        Self::DBError(Some(other))
    }
}

impl convert::From<actix::MailboxError> for HFMError {
    fn from(other: actix::MailboxError) -> Self {
        Self::ActorError(other)
    }
}

impl convert::From<tera::Error> for HFMError {
    fn from(other: tera::Error) -> Self {
        Self::TemplateError(other)
    }
}

impl convert::From<promptly::ReadlineError> for HFMError {
    fn from(other: promptly::ReadlineError) -> Self {
        let msg = format!("Error while prompting for configuration values: {:?}", other);
        Self::ConfigError(msg)
    }
}

impl std::fmt::Display for HFMError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.get_message_status().0)
    }
}

impl actix_web::ResponseError for HFMError {
    fn error_response(&self) -> HttpResponse {
        let (msg, status) = self.get_message_status();
        HttpResponse::build(status).body(String::from(msg))
    }
}
