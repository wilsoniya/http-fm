table! {
    fs_share (id) {
        id -> Integer,
        path -> Text,
        share_id -> Text,
        deleted -> Bool,
    }
}
