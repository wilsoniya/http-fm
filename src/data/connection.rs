use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

// provided by diesel_migrations
embed_migrations!();

use crate::errors::HFMError;

pub fn establish_connection(path: &str) -> Result<SqliteConnection, HFMError> {
    Ok(SqliteConnection::establish(path)?)
}

pub fn run_migrations(path: &str) -> Result<(), HFMError> {
    let connection = establish_connection(path)?;
    embedded_migrations::run_with_output(&connection, &mut std::io::stdout());
    Ok(())
}
