use std::path::PathBuf;

use actix::prelude::*;
use diesel::sqlite::SqliteConnection;
use diesel::prelude::*;
use uuid;

use crate::errors::HFMError;
use super::schema::fs_share::dsl::*;
use super::models;
use crate::Config;

/// Actor message requesting a FSShare.
pub struct MsgGetShare {
    pub share_id: String,
}

impl Message for MsgGetShare {
    type Result = Result<Option<models::FSShare>, HFMError>;
}

/// Actor message creating a FSShare
pub struct MsgCreateShare {
    pub path: PathBuf,
}

impl Message for MsgCreateShare {
    type Result = Result<models::FSShare, HFMError>;
}

/// Actor message listing all FSShares
pub struct MsgListShares;

impl Message for MsgListShares {
    type Result = Result<Vec<models::FSShare>, HFMError>;
}

pub struct DbExecutor(SqliteConnection);

impl DbExecutor {
    pub fn get_addr(db_url: String) -> Addr<Self> {
        // TODO: make number of workers parameterizable
        SyncArbiter::start(3, move || {
            let conn = super::connection::establish_connection(&db_url).unwrap();
            Self(conn)
        })
    }
}

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

impl Handler<MsgGetShare> for DbExecutor {
    type Result = Result<Option<models::FSShare>, HFMError>;

    fn handle(&mut self, msg: MsgGetShare, _: &mut Self::Context) -> Self::Result {
        fs_share
            .filter(share_id.eq(msg.share_id))
            .get_result::<models::FSShare>(&self.0)
            .optional()
            .map_err(HFMError::from)
    }
}

impl Handler<MsgCreateShare> for DbExecutor {
    type Result = Result<models::FSShare, HFMError>;

    fn handle(&mut self, msg: MsgCreateShare, _: &mut Self::Context) -> Self::Result {
        let path_str = msg.path
            .to_str()
            .ok_or(HFMError::InvalidPath)?
            .to_owned();

        let _share_id = uuid::Uuid::new_v4();
        let share_id_str = _share_id.to_hyphenated().to_string();
        let new_share = models::NewFSShare::new(path_str, Some(_share_id));

        diesel::insert_into(fs_share)
            .values(new_share)
            .execute(&self.0)
            .map_err(HFMError::from)
            .and_then(|_| {
                fs_share
                    .filter(share_id.eq(share_id_str))
                    .get_result::<models::FSShare>(&self.0)
                    .map_err(HFMError::from)
            })
    }
}

impl Handler<MsgListShares> for DbExecutor {
    type Result = Result<Vec<models::FSShare>, HFMError>;

    fn handle(&mut self, _: MsgListShares, _: &mut Self::Context) -> Self::Result {
        fs_share
            .get_results::<models::FSShare>(&self.0)
            .map_err(HFMError::from)
    }
}

/// A convenient asynchronous API covering the DbExecutor actor database
/// interface.
#[derive(Clone)]
pub struct Db(Addr<DbExecutor>);

impl Db {
    /// Builds a new `Db`.
    pub fn new(config: &Config) -> Self {
        Self(DbExecutor::get_addr(config.db_url.clone()))
    }

    pub async fn ls(&self) -> Result<Vec<models::FSShare>, HFMError> {
        let shares = self.0.send(MsgListShares{}).await??;
        Ok(shares)
    }

    pub async fn create_share(&self, _path: PathBuf) -> Result<models::FSShare, HFMError> {
        let share = self.0.send(MsgCreateShare{ path: _path }).await??;
        Ok(share)
    }

    pub async fn get_share(&self, _share_id: String) -> Result<Option<models::FSShare>, HFMError> {
        let maybe_share = self.0.send(MsgGetShare{ share_id: _share_id }).await??;
        Ok(maybe_share)
    }
}
