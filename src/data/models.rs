use uuid;

use super::schema::fs_share;

#[derive(Debug, Insertable)]
#[table_name="fs_share"]
pub struct NewFSShare {
    pub path: String,
    pub share_id: String,
    pub deleted: bool,
}

impl NewFSShare {
    pub fn new(path: String, maybe_share_id: Option<uuid::Uuid>) -> Self {
        let share_id = maybe_share_id.unwrap_or(uuid::Uuid::new_v4());
        Self {
            path: path,
            share_id: share_id.to_hyphenated().to_string(),
            deleted: false,
        }
    }
}

#[derive(Debug, Queryable)]
pub struct FSShare {
    pub id: i32,
    // TODO - would be nice if path: PathBuf
    pub path: String,
    pub share_id: String,
    pub deleted: bool,
}
