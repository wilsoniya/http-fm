use std::fs::create_dir_all;
use std::path::PathBuf;
use std::process::exit;

use clap::{
    App,
    Arg,
    SubCommand,
};
use env_logger::Env;
use log;
use promptly::prompt_default;

use http_fm::data::{actor, connection};
use http_fm::{
    Config,
    DEFAULT_LISTEN_ADDRESS,
    HFMError,
    format_share_url,
    get_data_dir,
};

#[macro_use] extern crate prettytable;
use prettytable::Table;

fn get_cli() -> App<'static, 'static> {
    App::new("HTTPFM server-side CLI")
        .arg(Arg::with_name("database-url")
            .value_name("DATABASE_URL")
            .env("DATABASE_URL")
            .help("Filesystem path to database file")
            .required(false)
        )
        .arg(Arg::with_name("url-root")
            .value_name("url-root")
            .short("u")
            .long("url-root")
            .env("URL_ROOT")
            .help("Public base url pointing to running http-fm daemon")
            .required(false)
        )
        .subcommand(SubCommand::with_name("share")
            .about("create a share from a filesystem path")
            .arg(Arg::with_name("path")
                .help("filesystem path of share")
                .required(true)
            )
        )
        .subcommand(SubCommand::with_name("ls")
            .about("list filesystem shares")
        )
        .subcommand(SubCommand::with_name("serve")
            .about("run the http-fm server")
            .arg(Arg::with_name("listen-address")
                .help("hostname:port on which to listen for TCP connections")
                .env("LISTEN_ADDRESS")
                .required(false)
            )
        )
        .subcommand(SubCommand::with_name("init")
            .about("initialize the http-fm configuration and database")
        )
}

fn get_config(matches: clap::ArgMatches<'static>) -> Result<Config, HFMError> {
    let maybe_config = Config::from_file();

    if let Err(HFMError::ConfigMissingError) = maybe_config {
        log::error!("httpfm configuration file does not exist");
        println!("Try running: `httpfm init` and then try again.");
        exit(1);
    }

    let config = maybe_config?;

    let db_url = matches.value_of("database-url")
        .unwrap_or(&config.db_url);
    let abs_db_path = std::fs::canonicalize(db_url)?;
    let abs_db_url = abs_db_path.to_string_lossy()
        .to_string();
    let listen_address = matches.subcommand_matches("serve")
        .and_then(|serve_matches| serve_matches.value_of("listen-address"))
        .map(String::from)
        .unwrap_or(config.listen_address);
    let url_root = matches.value_of("url-root")
        .map(String::from)
        .unwrap_or(config.url_root);

    Ok(Config::new(
        abs_db_url,
        url_root,
        listen_address,
    ))
}

fn init_config() -> Result<Config, HFMError> {
    let default_db_fpath = get_data_dir()?.join("db.sqlite");
    let base_url: String = prompt_default(
        "What is the fully-qualified public base URL for httpfm?",
        format!("http://{}", DEFAULT_LISTEN_ADDRESS),
    )?;
    let listen_address: String = prompt_default(
        "On which hostname:port should httpfm listen?",
        DEFAULT_LISTEN_ADDRESS.into(),
    )?;
    let db_url: PathBuf = prompt_default(
        "What's the absolute path to the httpfm database file?",
        default_db_fpath,
    )?;

    // ensure data directory exists
    db_url.parent()
        .ok_or_else(|| {
            HFMError::ConfigError("Unable to resolve directory containing database file".into())
        })
        .and_then(|db_dpath| {
            create_dir_all(db_dpath)
                .map_err(|err| HFMError::ConfigError(
                    format!(
                        "Unable to create data directory {:?} - {:?}",
                        db_dpath, err,
                    )
                ))
        })?;


    Config::new(
        db_url.to_string_lossy().to_string(),
        base_url,
        listen_address,
    ).to_file()
}

#[actix_rt::main]
async fn main() -> Result<(), HFMError> {
    env_logger::from_env(Env::default().default_filter_or("info")).init();
    let matches = get_cli().get_matches();

    if let ("init", Some(_)) = matches.subcommand() {
        let config = init_config()?;
        let _db = actor::Db::new(&config);
        connection::run_migrations(&config.db_url)?;
        log::info!("Initialized httpfm configuration and database.");
        return Ok(())
    }

    let config = get_config(matches.clone())?;

    log::trace!("DATABASE_URL: {}", config.db_url);
    log::trace!("URL_ROOT: {}", config.url_root);
    log::trace!("LISTEN_ADDRESS: {}", config.listen_address);

    let db = actor::Db::new(&config);

    connection::run_migrations(&config.db_url)?;

    match matches.subcommand() {
        ("share", Some(share_matches)) => {
            let share_path = share_matches.value_of("path")
                .expect("path parameter is required; this should never happen")
                .parse::<PathBuf>()
                .expect("PathBuf conversion is infallible; this should never happen");
            let abs_path = std::fs::canonicalize(share_path)?;
            let new_share = db.create_share(abs_path).await?;
            let share_url = format_share_url(&config, &new_share.share_id, None::<&str>);
            log::info!("created new share: {}", share_url);
            Ok(())
        },
        ("ls", Some(_)) => {
            let mut rows = db.ls().await?
                .iter()
                .map(|fs| row![fs.path, format_share_url(&config, &fs.share_id, None::<&str>), fs.deleted])
                .collect::<Vec<_>>();
            rows.insert(0, row!["FS Path", "Share URL", "Deleted"]);
            Table::init(rows).printstd();
            Ok(())
        },
        ("serve", Some(_)) => {
            http_fm::run_server(&config).await?;
            Ok(())
        },
        _ => Err(HFMError::CLIError("Subcommand required".into()))
    }
}
