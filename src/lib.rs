#[macro_use]
extern crate diesel;

#[macro_use]
extern crate diesel_migrations;

#[macro_use]
extern crate lazy_static;

use std::path::PathBuf;

use actix_web::{
    App,
    HttpServer,
    web,
};
use actix_web::middleware::Logger;

mod fs;
mod views;
pub mod data;
mod errors;
mod config;

pub use config::Config;
pub use config::DEFAULT_LISTEN_ADDRESS;
pub use config::get_data_dir;
pub use errors::HFMError;

pub async fn run_server(config: &config::Config) -> std::io::Result<()> {
    let db = data::actor::Db::new(config);

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .data(views::State::new(db.clone()))
            .route("/share/{id}/{fpath:.*}", web::get().to(views::share))
            .route("/share/{id}", web::get().to(views::share))
    })
    .bind(config.listen_address.clone())?
    .run()
    .await
}

/// Formats a public URL to the share identified by `id` and `maybe_path`.
pub fn format_share_url<T>(
    config: &config::Config,
    id: &str,
    maybe_path: Option<T>,
) -> String
where T: Into<PathBuf> {
    match maybe_path {
        Some(_path) => format!(
            "{root}/share/{id}{fpath}",
            id=id,
            fpath=PathBuf::from("/").join(_path.into()).to_string_lossy(),
            root=config.url_root,
        ),
        None => format!("{root}/share/{id}", id=id, root=config.url_root),
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn format_share_url_test() {
        let config = super::config::Config::new(
            "db_url".into(),
            "URL_ROOT".into(),
            "listen_address".into()
        );

        let expected = "URL_ROOT/share/id";
        let actual = super::format_share_url(&config, "id", None::<&str>);
        assert_eq!(expected, actual);

        let expected = "URL_ROOT/share/id/some/path";
        let actual = super::format_share_url(&config, "id", Some("/some/path"));
        assert_eq!(expected, actual);

        let expected = "URL_ROOT/share/id/some/path";
        let actual = super::format_share_url(&config, "id", Some("some/path"));
        assert_eq!(expected, actual);
    }
} /* test */

