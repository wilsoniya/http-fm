use std::fs::{File, OpenOptions, create_dir_all};
use std::path::PathBuf;
use std::io::{Read, Write};

use directories::ProjectDirs;
use log;
use serde::{Serialize, Deserialize};
use toml;

use crate::errors::HFMError;

pub static DEFAULT_LISTEN_ADDRESS: &'static str = "127.0.0.1:8088";

/// Configuration for http-fm.
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub db_url: String,
    pub url_root: String,
    pub listen_address: String
}

impl Config {
    pub fn new(
        db_url: String,
        url_root: String,
        listen_address: String,
    ) -> Self {
        Config {
            db_url,
            url_root,
            listen_address,
        }
    }

    pub fn from_file() -> Result<Self, HFMError> {
        get_config_dir()
            .and_then(|config_dir| {
                let config_fpath = config_dir.join("config.toml");
                let mut file = File::open(config_fpath.clone())
                    .map_err(|_| HFMError::ConfigMissingError)?;

                let mut config_body = String::new();
                file.read_to_string(&mut config_body)
                    .map_err(|err| {
                        let msg = format!("Unable to read configuration file: {:?} - {:?}", config_fpath, err);
                        HFMError::ConfigError(msg)
                    })?;

                toml::from_str(&config_body)
                    .map_err(|err| {
                        let msg = format!("Unable to interpret configuration file: {:?} - {:?}", config_fpath, err);
                        HFMError::ConfigError(msg)
                    })
            })
    }

    #[allow(unused_must_use)]
    pub fn to_file(self) -> Result<Self, HFMError> {
        let config_body = toml::to_string_pretty(&self)
            .map_err(|err| {
                let msg = format!("Error generating config file: {:?}", err);
                HFMError::ConfigError(msg)
            })?;

        get_config_dir()
            .and_then(|config_dir| {
                create_dir_all(&config_dir)
                    .map_err(|_| {
                        let msg = format!("Unable to create directory containing configuration file: {:?}", &config_dir);
                        HFMError::ConfigError(msg)
                    })?;

                let config_fpath = config_dir.join("config.toml");
                let mut file = OpenOptions::new()
                    .write(true)
                    .create(true)
                    .open(config_fpath.clone())
                    .map_err(|err| {
                        let msg = format!("Unable to open configuration file: {:?} - {:?}", config_fpath, err);
                        HFMError::ConfigError(msg)
                    })?;

                file.write_all(config_body.as_bytes())
                    .map_err(|err| {
                        let msg = format!("Unable to write configuration file: {:?} - {:?}", config_fpath, err);
                        HFMError::ConfigError(msg)
                    });

                log::info!("Wrote config file: {:?}", config_fpath);

                Ok(self)
            })
    }
}

fn get_config_dir() -> Result<PathBuf, HFMError> {
    ProjectDirs::from("org", "wilsoniya", "httpfm")
        .ok_or_else(|| {
            HFMError::ConfigError("Unable to establish configuration directory".into())
        })
        .map(|proj_dir| proj_dir.config_dir().into())
}

pub fn get_data_dir() -> Result<PathBuf, HFMError> {
    ProjectDirs::from("org", "wilsoniya", "httpfm")
        .ok_or_else(|| {
            HFMError::ConfigError("Unable to establish data directory".into())
        })
        .map(|proj_dir| proj_dir.data_dir().into())
}

